package org.pragmaticmodeling.pxdoc.eclipse.bpmn2.gen.ui;

import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.AbstractPxUiPlugin;

public class EclipseBpmn2GeneratorUiModule extends AbstractEclipseBpmn2GeneratorUiModule {
		   
	public EclipseBpmn2GeneratorUiModule(AbstractPxUiPlugin plugin) {
		super(plugin);
	}
	
}
	
