package org.pragmaticmodeling.pxdoc.eclipse.bpmn2.gen.ui;

import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.bpmn2.AbstractEclipseBpmn2PropertyTester;
import org.eclipse.bpmn2.Definitions;

/**
 * PxDocModel property tester and model provider
 */
public class AbstractEclipseBpmn2GeneratorPropertyTester extends AbstractEclipseBpmn2PropertyTester {

	public AbstractEclipseBpmn2GeneratorPropertyTester() {
		super(Definitions.class);
	}
}

