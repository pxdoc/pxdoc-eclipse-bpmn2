package org.pragmaticmodeling.pxdoc.eclipse.bpmn2.gen.ui;

import org.osgi.framework.Bundle;

import com.google.inject.Injector;

import fr.pragmaticmodeling.pxdoc.runtime.eclipse.guice.AbstractGuiceAwareExecutableExtensionFactory;

public class EclipseBpmn2GeneratorExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

	@Override
	   protected Bundle getBundle() {
	       return EclipseBpmn2GeneratorActivator.getInstance().getBundle();
	   }
	    
	   @Override
	   protected Injector getInjector() {
	       return EclipseBpmn2GeneratorActivator.getInstance().getInjector();
	   }
	   
}
	
