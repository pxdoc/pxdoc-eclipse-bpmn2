package org.pragmaticmodeling.pxdoc.eclipse.bpmn2.gen;
				
import com.google.inject.Binder;
import com.google.inject.Module;
				
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
					
public abstract class AbstractEclipseBpmn2GeneratorModule implements Module {
					
	@Override
	public void configure(Binder binder) {
		binder.bind(IPxDocGenerator.class).to(EclipseBpmn2Generator.class);
	}

}