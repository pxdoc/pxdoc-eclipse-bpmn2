package org.pragmaticmodeling.pxdoc.eclipse.bpmn2.gen;

import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import fr.pragmaticmodeling.pxdoc.ContainerElement;
import fr.pragmaticmodeling.pxdoc.Document;
import fr.pragmaticmodeling.pxdoc.HeadingN;
import fr.pragmaticmodeling.pxdoc.UnitKind;
import fr.pragmaticmodeling.pxdoc.runtime.AbstractPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.PxDocGenerationException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;
import org.eclipse.bpmn2.BaseElement;
import org.eclipse.bpmn2.Definitions;
import org.eclipse.bpmn2.FlowElement;
import org.eclipse.bpmn2.FlowNode;
import org.eclipse.bpmn2.StartEvent;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;
import org.pragmaticmodeling.pxdoc.common.lib.CommonServices;
import org.pragmaticmodeling.pxdoc.common.lib.IDescriptionProvider;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramProvider;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramQueryBuilder;
import org.pragmaticmodeling.pxdoc.diagrams.lib.DiagramServices;
import org.pragmaticmodeling.pxdoc.eclipse.bpmn2.lib.EclipseBpmn2Services;
import org.pragmaticmodeling.pxdoc.eclipse.bpmn2.lib.FlowNodesComparator;
import org.pragmaticmodeling.pxdoc.emf.lib.EmfServices;
import org.pragmaticmodeling.pxdoc.runtime.eclipse.bpmn2.BpmnDiagramHelper;

@SuppressWarnings("all")
public class EclipseBpmn2Generator extends AbstractPxDocGenerator {
  private Logger logger = Logger.getLogger(getClass());
  
  private List<String> _stylesToBind;
  
  public CommonServices _CommonServices = (CommonServices)getGenerator().getModule(CommonServices.class);
  
  public String getDescription(final Object element) {
    return _CommonServices.getDescription(element);
  }
  
  public IDescriptionProvider getDescriptionProvider() {
    return _CommonServices.getDescriptionProvider();
  }
  
  public boolean getSuggestImprovements() {
    return _CommonServices.getSuggestImprovements();
  }
  
  public boolean hasBookmark(final Object object) {
    return _CommonServices.hasBookmark(object);
  }
  
  public boolean isEmptyDocumentation(final String text) {
    return _CommonServices.isEmptyDocumentation(text);
  }
  
  public void setDescriptionProvider(final IDescriptionProvider descProvider) {
    _CommonServices.setDescriptionProvider(descProvider);
  }
  
  public void setSuggestImprovements(final boolean value) {
    _CommonServices.setSuggestImprovements(value);
  }
  
  public String suffixedFile(final String doc, final String suffix) {
    return _CommonServices.suffixedFile(doc, suffix);
  }
  
  public String validBookmark(final Object object) {
    return _CommonServices.validBookmark(object);
  }
  
  public DiagramServices _DiagramServices = (DiagramServices)getGenerator().getModule(DiagramServices.class);
  
  public IDiagramProvider<? extends IDiagramQueryBuilder> getDiagramProvider() {
    return _DiagramServices.getDiagramProvider();
  }
  
  public IDiagramQueryBuilder queryDiagrams(final Object namespace) {
    return _DiagramServices.queryDiagrams(namespace);
  }
  
  public void setDiagramProvider(final IDiagramProvider<? extends IDiagramQueryBuilder> diagramProvider) {
    _DiagramServices.setDiagramProvider(diagramProvider);
  }
  
  public EclipseBpmn2Services _EclipseBpmn2Services = (EclipseBpmn2Services)getGenerator().getModule(EclipseBpmn2Services.class);
  
  public String description(final BaseElement be) {
    return _EclipseBpmn2Services.description(be);
  }
  
  public BufferedImage getCroppedImage(final FlowNode element) {
    return _EclipseBpmn2Services.getCroppedImage(element);
  }
  
  public BpmnDiagramHelper getDiagramHelper() {
    return _EclipseBpmn2Services.getDiagramHelper();
  }
  
  public FlowNodesComparator getFlowNodesComparator() {
    return _EclipseBpmn2Services.getFlowNodesComparator();
  }
  
  public boolean isGenerated(final FlowElement fe) {
    return _EclipseBpmn2Services.isGenerated(fe);
  }
  
  public void setCropedImagesRatio(final float ratio) {
    _EclipseBpmn2Services.setCropedImagesRatio(ratio);
  }
  
  public void setDiagramHelper(final BpmnDiagramHelper helper) {
    _EclipseBpmn2Services.setDiagramHelper(helper);
  }
  
  public void setFlowNodesComparator(final FlowNodesComparator comparator) {
    _EclipseBpmn2Services.setFlowNodesComparator(comparator);
  }
  
  public EmfServices _EmfServices = (EmfServices)getGenerator().getModule(EmfServices.class);
  
  public List<EObject> getChildren(final EObject eObject) {
    return _EmfServices.getChildren(eObject);
  }
  
  public Set<EClass> getExcludedEClasses() {
    return _EmfServices.getExcludedEClasses();
  }
  
  public Set<EStructuralFeature> getExcludedFeatures() {
    return _EmfServices.getExcludedFeatures();
  }
  
  public String getHyperlinkComment(final EObject eObject) {
    return _EmfServices.getHyperlinkComment(eObject);
  }
  
  public BufferedImage getImage(final Object object) {
    return _EmfServices.getImage(object);
  }
  
  public String getPrettyName(final EStructuralFeature feature) {
    return _EmfServices.getPrettyName(feature);
  }
  
  public List<EStructuralFeature> getProperties(final EObject eObject) {
    return _EmfServices.getProperties(eObject);
  }
  
  public String getText(final EObject eObject) {
    return _EmfServices.getText(eObject);
  }
  
  public boolean isEProperty(final EStructuralFeature feature, final EObject eObject) {
    return _EmfServices.isEProperty(feature, eObject);
  }
  
  public boolean isExcluded(final EObject element) {
    return _EmfServices.isExcluded(element);
  }
  
  public void setLabelProvider(final ILabelProvider labelProvider) {
    _EmfServices.setLabelProvider(labelProvider);
  }
  
  public EclipseBpmn2Generator() {
    super();
  }
  
  public void generate() throws PxDocGenerationException {
    org.eclipse.bpmn2.Definitions model = (org.eclipse.bpmn2.Definitions)launcher.getModel().getInstance();
    try {
    	logger.info("----------------------------------------------------------------");
    	logger.info("Starting EclipseBpmn2Generator generation...");
    	main(null, model);
    } catch (Exception e) {
    	throw new PxDocGenerationException(e);
    } catch (NoClassDefFoundError e) {
        logger.error(e.getMessage());
    } finally {
    	logger.info("FINISHED: EclipseBpmn2Generator generation...");
    }
  }
  
  public List<String> getStylesToBind() {
    if (_stylesToBind == null) {
    	_stylesToBind = new ArrayList<String>();
    	_stylesToBind.add("Emphasis");					
    	_stylesToBind.add("BulletList");					
    	_stylesToBind.add("BodyText");					
    	_stylesToBind.add("Figure");					
    }
    return _stylesToBind;
  }
  
  public void error(final String message) {
    logger.error(message);
  }
  
  public void info(final String message) {
    logger.info(message);
  }
  
  /**
   * Entry point.
   */
  public void main(final ContainerElement parent, final Definitions model) throws PxDocGenerationException {
    StartEvent startEvent = IteratorExtensions.<StartEvent>head(Iterators.<StartEvent>filter(model.eAllContents(), StartEvent.class));
    FlowNodesComparator _flowNodesComparator = new FlowNodesComparator(startEvent);
    this.setFlowNodesComparator(_flowNodesComparator);
    BpmnDiagramHelper _bpmnDiagramHelper = new BpmnDiagramHelper(model);
    this.setDiagramHelper(_bpmnDiagramHelper);
    org.eclipse.bpmn2.Process process = IterableExtensions.<org.eclipse.bpmn2.Process>head(Iterables.<org.eclipse.bpmn2.Process>filter(model.getRootElements(), org.eclipse.bpmn2.Process.class));
    String _file = (new File(getLauncher().getTargetFile())).getAbsolutePath();
    Document lObj0 = createDocument(parent, _file, getStylesheetName(), null, createMeasure(getHeaderHeight(), UnitKind.CM), createMeasure(getFooterHeight(), UnitKind.CM));
    setDocumentProperty(lObj0, "title", process.getName());
    addDocument(lObj0);
    {
      createToc(lObj0, null, "", null);
      createParagraphBreak(lObj0);
      createPageBreak(lObj0);
      HeadingN lObj1 = createHeadingN(lObj0, 1, false);
      createText(lObj1, "Process overview");
      List<BufferedImage> exportedImagesWithSplit = this.getDiagramHelper().exportDiagram(
        this.getGenerator().getStylesheet().getAvailableWidth(), this.getGenerator().getStylesheet().getAvailableHeight(), true);
      for (final BufferedImage img : exportedImagesWithSplit) {
        {
          createImage(lObj0, null, img, null, null, null, null);
          createLineBreak(lObj0);
        }
      }
      HeadingN lObj2 = createHeadingN(lObj0, 1, false);
      createText(lObj2, "Process steps");
      Iterator<org.eclipse.bpmn2.FlowNode> lObj3 = getIterator(IterableExtensions.<FlowNode>sortWith(Iterables.<FlowNode>filter(process.getFlowElements(), FlowNode.class), this.getFlowNodesComparator()));
      while (lObj3.hasNext()) {
        _EclipseBpmn2Services.flowNode(lObj0, lObj3.next(), 2);
        
      }
    }
  }
}
