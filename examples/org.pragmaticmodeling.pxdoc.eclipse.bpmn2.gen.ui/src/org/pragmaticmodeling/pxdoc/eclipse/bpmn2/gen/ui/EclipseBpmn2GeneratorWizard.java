package org.pragmaticmodeling.pxdoc.eclipse.bpmn2.gen.ui;

import org.eclipse.emf.ecore.EObject;

/**
 * Wizard class
 */
public class EclipseBpmn2GeneratorWizard extends AbstractEclipseBpmn2GeneratorWizard {

	@Override
	public String getModelIdentifier(Object model) {
		return ((EObject)model).eResource().getURI().toString();
	}

}
	 
	
