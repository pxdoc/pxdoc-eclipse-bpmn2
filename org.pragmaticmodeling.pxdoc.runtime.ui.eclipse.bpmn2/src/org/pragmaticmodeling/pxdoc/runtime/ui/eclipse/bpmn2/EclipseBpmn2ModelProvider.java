package org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.bpmn2;

import org.eclipse.bpmn2.Definitions;
import org.eclipse.bpmn2.modeler.core.model.ModelHandler;
import org.eclipse.bpmn2.modeler.core.model.ModelHandlerLocator;
import org.eclipse.bpmn2.modeler.core.utils.ErrorUtils;
import org.eclipse.bpmn2.modeler.core.utils.FileUtils;
import org.eclipse.bpmn2.modeler.ui.Bpmn2DiagramEditorInput;
import org.eclipse.bpmn2.modeler.ui.editor.BPMN2Editor;
import org.eclipse.bpmn2.modeler.ui.wizards.Messages;
import org.eclipse.bpmn2.util.Bpmn2ResourceFactoryImpl;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.Resource.Factory;
import org.eclipse.graphiti.ui.editor.DiagramEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import fr.pragmaticmodeling.pxdoc.runtime.IModelProvider;
import fr.pragmaticmodeling.pxdoc.util.EcoreUtil2;

public class EclipseBpmn2ModelProvider implements IModelProvider {

	@Override
	public Object adaptSelection(Object selection) {
		if (selection instanceof IFile && ((IFile) selection).getFileExtension().startsWith("bpmn")) {
			URI uri = URI.createPlatformResourceURI(((IFile) selection).getFullPath().toPortableString(), true);
			Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
			reg.getExtensionToFactoryMap().put("bpmn2", new Bpmn2ResourceFactoryImpl());
			reg.getExtensionToFactoryMap().put("bpmn", new Bpmn2ResourceFactoryImpl());
			Factory f = reg.getFactory(uri);
			String diagramName = FileUtils.createTempName(uri.toString());
			URI diagramUri = URI.createFileURI(diagramName);
			Bpmn2DiagramEditorInput editorInput = new Bpmn2DiagramEditorInput(uri, diagramUri,
					"org.eclipse.bpmn2.modeler.ui.diagram.MainBPMNDiagramType");
			openEditor(editorInput);
			Resource res = f.createResource(uri);
			ModelHandler modelHandler = ModelHandlerLocator.getModelHandler(res);
			if (modelHandler != null)
				return modelHandler.getDefinitions();
		} else if (selection instanceof EObject) {
			return EcoreUtil2.getContainerOfType((EObject) selection, Definitions.class);
		}
		throw new RuntimeException("Could not load bpmn model.");
	}

	public static IEditorPart openEditor(final DiagramEditorInput editorInput) {
		final Object result[] = { new Object() };
		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {
			@Override
			public void run() {
				try {
					IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
					IEditorPart part = null;
					part = page.findEditor(editorInput);
					if (part != null) {
						page.activate(part);
					} else {
						part = page.openEditor(editorInput, BPMN2Editor.EDITOR_ID);
					}
					result[0] = part;
				} catch (PartInitException e) {
					String error = Messages.BPMN2DiagramCreator_Create_Error;
					IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, error, e);
					ErrorUtils.showErrorWithLogging(status);
				}
			}
		});
		return (IEditorPart) result[0];
	}

}
