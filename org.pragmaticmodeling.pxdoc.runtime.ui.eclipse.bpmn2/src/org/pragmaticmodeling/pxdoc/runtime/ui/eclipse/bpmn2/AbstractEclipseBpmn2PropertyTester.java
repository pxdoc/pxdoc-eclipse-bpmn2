package org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.bpmn2;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.ecore.EObject;
import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.commands.DefaultPropertyTester;

import fr.pragmaticmodeling.pxdoc.util.EcoreUtil2;

public class AbstractEclipseBpmn2PropertyTester extends DefaultPropertyTester{

	public AbstractEclipseBpmn2PropertyTester(Class<?> clazz) {
		super(clazz);
	}
	
	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {
		if (receiver instanceof IFile) {
			return ((IFile)receiver).getFileExtension().startsWith("bpmn");
		} else if (receiver instanceof EObject) {
			return EcoreUtil2.getContainerOfType((EObject)receiver, (Class)getClass_()) != null;
		}
		return super.test(receiver, property, args, expectedValue);
	}
	
}
