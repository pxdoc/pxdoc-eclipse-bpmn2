package org.pragmaticmodeling.pxdoc.eclipse.bpmn2.lib;

import org.eclipse.bpmn2.BaseElement;
import org.eclipse.bpmn2.Documentation;
import org.pragmaticmodeling.pxdoc.common.lib.DefaultDescriptionProvider;

import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;

public class EclipseBpmn2DescriptionProvider extends DefaultDescriptionProvider {

	public EclipseBpmn2DescriptionProvider(IPxDocGenerator generator) {
		super(generator);
	}
	
	@Override
	public String getDescription(Object object) {
		if (object instanceof BaseElement) {
			BaseElement be = (BaseElement)object;
			String doc = "";
			for (Documentation docElt : be.getDocumentation()) {
				doc += docElt.getText() + "\n"; 
			}
			if (doc.length()>0)
			doc = doc.substring(0, doc.length()-1); 
			return doc;
		}
		return super.getDescription(object);
	}
	
	

	
}
