package org.pragmaticmodeling.pxdoc.eclipse.bpmn2.lib;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.bpmn2.FlowNode;
import org.eclipse.bpmn2.SequenceFlow;
import org.eclipse.bpmn2.StartEvent;

public class FlowNodesComparator implements java.util.Comparator<FlowNode> {

	private Map<FlowNode, Integer> map = new HashMap<FlowNode, Integer>();

	public FlowNodesComparator(StartEvent startEvent) {
		super();
		init(startEvent);
	}

	private void init(StartEvent startEvent) {
		int depth = 1;
		List<FlowNode> visited = new ArrayList<FlowNode>();

		visitNode(startEvent, depth + 1, visited);

	}

	private void visitNode(FlowNode node, int depth, List<FlowNode> visited) {
		if (!visited.contains(node)) {
			map.put(node, depth);
			visited.add(node);
			for (SequenceFlow flow : node.getOutgoing()) {
				visitNode(flow.getTargetRef(), depth+1, visited);
			}
		}
	}

	@Override
	public int compare(FlowNode o1, FlowNode o2) {
		return Integer.compare(getDepth(o1), getDepth(o2));
	}

	private int getDepth(FlowNode node) {
//		if (!map.containsKey(node)) {
//			List<FlowNode> visited = new ArrayList<FlowNode>();
//			int depth = getDepth(node, visited);
//			// int depth = 1;
//			//
//			// for (SequenceFlow flow : node.getIncoming()) {
//			// System.out.println(node.getName());
//			// depth += getDepth(flow.getSourceRef());
//			// }
//			map.put(node, depth);
//		}
		if (!map.containsKey(node))
			return 99999;
		return map.get(node);
	}

//	private int getDepth(FlowNode node, List<FlowNode> visited) {
//		visited.add(node);
//		if (!map.containsKey(node)) {
//			int depth = 1;
//			for (SequenceFlow flow : node.getIncoming()) {
//				System.out.println(node.getName());
//				if (!visited.contains(flow.getSourceRef())) {
//					depth += getDepth(flow.getSourceRef(), visited);
//				}
//			}
//			map.put(node, depth);
//		}
//		return map.get(node);
//	}

}
