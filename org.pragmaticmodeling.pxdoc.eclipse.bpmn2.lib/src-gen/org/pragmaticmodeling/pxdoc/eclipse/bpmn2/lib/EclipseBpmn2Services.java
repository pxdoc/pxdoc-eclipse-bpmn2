package org.pragmaticmodeling.pxdoc.eclipse.bpmn2.lib;

import fr.pragmaticmodeling.pxdoc.AlignmentType;
import fr.pragmaticmodeling.pxdoc.Bold;
import fr.pragmaticmodeling.pxdoc.Bookmark;
import fr.pragmaticmodeling.pxdoc.Cell;
import fr.pragmaticmodeling.pxdoc.ContainerElement;
import fr.pragmaticmodeling.pxdoc.Font;
import fr.pragmaticmodeling.pxdoc.HeadingN;
import fr.pragmaticmodeling.pxdoc.Hyperlink;
import fr.pragmaticmodeling.pxdoc.Measure;
import fr.pragmaticmodeling.pxdoc.Row;
import fr.pragmaticmodeling.pxdoc.ShadingPattern;
import fr.pragmaticmodeling.pxdoc.StyleApplication;
import fr.pragmaticmodeling.pxdoc.Table;
import fr.pragmaticmodeling.pxdoc.UnitKind;
import fr.pragmaticmodeling.pxdoc.common.MeasuresConverter;
import fr.pragmaticmodeling.pxdoc.runtime.AbstractPxDocModule;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.PxDocGenerationException;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;
import org.eclipse.bpmn2.BaseElement;
import org.eclipse.bpmn2.ComplexGateway;
import org.eclipse.bpmn2.DataObject;
import org.eclipse.bpmn2.Documentation;
import org.eclipse.bpmn2.Expression;
import org.eclipse.bpmn2.FlowElement;
import org.eclipse.bpmn2.FlowNode;
import org.eclipse.bpmn2.FormalExpression;
import org.eclipse.bpmn2.GatewayDirection;
import org.eclipse.bpmn2.ParallelGateway;
import org.eclipse.bpmn2.SequenceFlow;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.viewers.ILabelProvider;
import org.pragmaticmodeling.pxdoc.common.lib.CommonServices;
import org.pragmaticmodeling.pxdoc.common.lib.IDescriptionProvider;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramProvider;
import org.pragmaticmodeling.pxdoc.diagrams.IDiagramQueryBuilder;
import org.pragmaticmodeling.pxdoc.diagrams.lib.DiagramServices;
import org.pragmaticmodeling.pxdoc.eclipse.bpmn2.lib.FlowNodesComparator;
import org.pragmaticmodeling.pxdoc.emf.lib.EmfServices;
import org.pragmaticmodeling.pxdoc.runtime.eclipse.bpmn2.BpmnDiagramHelper;

@SuppressWarnings("all")
public class EclipseBpmn2Services extends AbstractPxDocModule {
  private Logger logger = Logger.getLogger(getClass());
  
  public FlowNodesComparator flowNodeComparator;
  
  public BpmnDiagramHelper diagramHelper;
  
  public float cropedImagesRatio = 1.0f;
  
  private List<String> _stylesToBind;
  
  public CommonServices _CommonServices = (CommonServices)getGenerator().getModule(CommonServices.class);
  
  public String getDescription(final Object element) {
    return _CommonServices.getDescription(element);
  }
  
  public IDescriptionProvider getDescriptionProvider() {
    return _CommonServices.getDescriptionProvider();
  }
  
  public boolean getSuggestImprovements() {
    return _CommonServices.getSuggestImprovements();
  }
  
  public boolean hasBookmark(final Object object) {
    return _CommonServices.hasBookmark(object);
  }
  
  public boolean isEmptyDocumentation(final String text) {
    return _CommonServices.isEmptyDocumentation(text);
  }
  
  public void setDescriptionProvider(final IDescriptionProvider descProvider) {
    _CommonServices.setDescriptionProvider(descProvider);
  }
  
  public void setSuggestImprovements(final boolean value) {
    _CommonServices.setSuggestImprovements(value);
  }
  
  public String suffixedFile(final String doc, final String suffix) {
    return _CommonServices.suffixedFile(doc, suffix);
  }
  
  public String validBookmark(final Object object) {
    return _CommonServices.validBookmark(object);
  }
  
  public DiagramServices _DiagramServices = (DiagramServices)getGenerator().getModule(DiagramServices.class);
  
  public IDiagramProvider<? extends IDiagramQueryBuilder> getDiagramProvider() {
    return _DiagramServices.getDiagramProvider();
  }
  
  public IDiagramQueryBuilder queryDiagrams(final Object namespace) {
    return _DiagramServices.queryDiagrams(namespace);
  }
  
  public void setDiagramProvider(final IDiagramProvider<? extends IDiagramQueryBuilder> diagramProvider) {
    _DiagramServices.setDiagramProvider(diagramProvider);
  }
  
  public EmfServices _EmfServices = (EmfServices)getGenerator().getModule(EmfServices.class);
  
  public List<EObject> getChildren(final EObject eObject) {
    return _EmfServices.getChildren(eObject);
  }
  
  public Set<EClass> getExcludedEClasses() {
    return _EmfServices.getExcludedEClasses();
  }
  
  public Set<EStructuralFeature> getExcludedFeatures() {
    return _EmfServices.getExcludedFeatures();
  }
  
  public String getHyperlinkComment(final EObject eObject) {
    return _EmfServices.getHyperlinkComment(eObject);
  }
  
  public BufferedImage getImage(final Object object) {
    return _EmfServices.getImage(object);
  }
  
  public String getPrettyName(final EStructuralFeature feature) {
    return _EmfServices.getPrettyName(feature);
  }
  
  public List<EStructuralFeature> getProperties(final EObject eObject) {
    return _EmfServices.getProperties(eObject);
  }
  
  public String getText(final EObject eObject) {
    return _EmfServices.getText(eObject);
  }
  
  public boolean isEProperty(final EStructuralFeature feature, final EObject eObject) {
    return _EmfServices.isEProperty(feature, eObject);
  }
  
  public boolean isExcluded(final EObject element) {
    return _EmfServices.isExcluded(element);
  }
  
  public void setLabelProvider(final ILabelProvider labelProvider) {
    _EmfServices.setLabelProvider(labelProvider);
  }
  
  public EclipseBpmn2Services(final IPxDocGenerator generator) {
    super(generator);
  }
  
  public void setCropedImagesRatio(final float ratio) {
    this.cropedImagesRatio = ratio;
  }
  
  public FlowNodesComparator getFlowNodesComparator() {
    return this.flowNodeComparator;
  }
  
  public void setFlowNodesComparator(final FlowNodesComparator comparator) {
    this.flowNodeComparator = comparator;
  }
  
  public BpmnDiagramHelper getDiagramHelper() {
    return this.diagramHelper;
  }
  
  public void setDiagramHelper(final BpmnDiagramHelper helper) {
    this.diagramHelper = helper;
  }
  
  public String description(final BaseElement be) {
    String result = "";
    List<Documentation> _documentation = be.getDocumentation();
    for (final Documentation doc : _documentation) {
      String _result = result;
      String _text = doc.getText();
      result = (_result + _text);
    }
    if (((result.length() > 0) && (!result.endsWith(".")))) {
      String _result_1 = result;
      result = (_result_1 + ".");
    }
    return result;
  }
  
  public boolean isGenerated(final FlowElement fe) {
    return ((fe instanceof FlowNode) || (fe instanceof DataObject));
  }
  
  public BufferedImage getCroppedImage(final FlowNode element) {
    if ((this.diagramHelper != null)) {
      return this.diagramHelper.cropImage(element);
    }
    return null;
  }
  
  public void error(final String message) {
    logger.error(message);
  }
  
  public void info(final String message) {
    logger.info(message);
  }
  
  public void specializedDescription(final ContainerElement parent, final FlowNode element) throws PxDocGenerationException {
    boolean _matched = false;
    if (element instanceof ParallelGateway) {
      _matched=true;
      StyleApplication lObj0 = createStyleApplication(parent, false, getBindedStyle("BodyText"), null, null, null);
      GatewayDirection _gatewayDirection = ((ParallelGateway)element).getGatewayDirection();
      if (_gatewayDirection != null) {
        switch (_gatewayDirection) {
          case DIVERGING:
            createText(lObj0, "A diverging parallel gateway.");
            break;
          case CONVERGING:
            createText(lObj0, "A converging parallel gateway.");
            break;
          default:
            break;
        }
      } else {
      }
    }
    if (!_matched) {
      if (element instanceof ComplexGateway) {
        _matched=true;
        StyleApplication lObj1 = createStyleApplication(parent, false, getBindedStyle("BodyText"), null, null, null);
        {
          Expression _activationCondition = ((ComplexGateway)element).getActivationCondition();
          FormalExpression condition = ((FormalExpression) _activationCondition);
          if ((condition != null)) {
            Bold lObj2 = createBold(lObj1);
            createText(lObj2, "Activation condition: ");
            createText(lObj1, condition.getBody());
          }
        }
      }
    }
    if (!_matched) {
      StyleApplication lObj3 = createStyleApplication(parent, false, getBindedStyle("BodyText"), null, null, null);
      createText(lObj3, this.description(element));
    }
    BufferedImage cropedImage = this.getCroppedImage(element);
    if ((cropedImage != null)) {
      StyleApplication lObj4 = createStyleApplication(parent, false, getBindedStyle("BodyText"), AlignmentType.CENTER, null, null);
      {
        createParagraphBreak(lObj4);
        int _width = cropedImage.getWidth();
        float _multiply = (_width * this.cropedImagesRatio);
        int _measureValueInPixels = MeasuresConverter.getMeasureValueInPixels(this.generator.getAvailableWidth(), "TWIPS", this.generator.getAvailableWidth());
        boolean exceedsWidth = (_multiply > _measureValueInPixels);
        int _height = cropedImage.getHeight();
        float _multiply_1 = (_height * this.cropedImagesRatio);
        int _measureValueInPixels_1 = MeasuresConverter.getMeasureValueInPixels(this.generator.getAvailableHeight(), "TWIPS", this.generator.getAvailableWidth());
        boolean exceedsHeight = (_multiply_1 > _measureValueInPixels_1);
        if (((!exceedsHeight) && (!exceedsWidth))) {
          int _width_1 = cropedImage.getWidth();
          float _multiply_2 = (_width_1 * this.cropedImagesRatio);
          int width = ((int) _multiply_2);
          int _height_1 = cropedImage.getHeight();
          float _multiply_3 = (_height_1 * this.cropedImagesRatio);
          int height = ((int) _multiply_3);
          Measure imageWidth5 = createMeasure((float)width, UnitKind.PIXELS);
          Measure imageHeight6 = createMeasure((float)height, UnitKind.PIXELS);
          createImage(lObj4, null, cropedImage, imageWidth5, imageHeight6, null, null);
        } else {
          createImage(lObj4, null, cropedImage, null, null, null, null);
        }
      }
    }
  }
  
  public void flowNode(final ContainerElement parent, final FlowNode element, final int level) throws PxDocGenerationException {
    HeadingN lObj0 = createHeadingN(parent, level, false);
    String lObj1 = element.getId();
    Bookmark lObj2 = createBookmark(lObj0, getBookmarkString(lObj1));
    _EmfServices.iconAndText(lObj2, element);
    specializedDescription(parent, element);
    createParagraphBreak(parent);
    Table lObj3 = createTable(parent, null, null, null, null, ShadingPattern.NO_VALUE);
    {
      Row lObj4 = createRow(lObj3, null, false, "192,192,192", ShadingPattern.NO_VALUE, false, null);
      {
        Cell lObj5 = createCell(lObj4);
        createText(lObj5, "Incoming");
        Cell lObj6 = createCell(lObj4);
        createText(lObj6, "Outgoing");
      }
      Row lObj7 = createRow(lObj3, null, false, null, ShadingPattern.NO_VALUE, false, null);
      {
        Cell lObj8 = createCell(lObj7);
        List<SequenceFlow> _incoming = element.getIncoming();
        for (final SequenceFlow e : _incoming) {
          StyleApplication lObj9 = createStyleApplication(lObj8, false, getBindedStyle("BodyText"), null, null, null);
          Font lObj10 = createFont(lObj9, null, 10, false, false, false, null, false, null, null, null);
          Hyperlink lObj11 = createHyperlink(lObj10, getBookmarkString(e.getSourceRef().getId()), this.description(e.getSourceRef()), null);
          _EmfServices.iconAndText(lObj11, e.getSourceRef());
        }
        Cell lObj12 = createCell(lObj7);
        List<SequenceFlow> _outgoing = element.getOutgoing();
        for (final SequenceFlow e_1 : _outgoing) {
          StyleApplication lObj13 = createStyleApplication(lObj12, false, getBindedStyle("BodyText"), null, null, null);
          Font lObj14 = createFont(lObj13, null, 10, false, false, false, null, false, null, null, null);
          Hyperlink lObj15 = createHyperlink(lObj14, getBookmarkString(e_1.getTargetRef().getId()), null, null);
          _EmfServices.iconAndText(lObj15, e_1.getTargetRef());
        }
      }
    }
  }
}
