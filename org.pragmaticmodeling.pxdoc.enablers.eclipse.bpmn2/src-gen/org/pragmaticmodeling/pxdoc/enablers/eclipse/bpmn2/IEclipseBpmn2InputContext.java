package org.pragmaticmodeling.pxdoc.enablers.eclipse.bpmn2;

import org.pragmaticmodeling.pxdoc.enablers.eclipse.IEclipseUiContext;

@SuppressWarnings("all")
public interface IEclipseBpmn2InputContext extends IEclipseUiContext {
}
