package org.pragmaticmodeling.pxdoc.enablers.capella;

import com.google.inject.Injector;
import fr.pragmaticmodeling.pxdoc.dsl.pxDoc.PxDocGenerator;
import fr.pragmaticmodeling.pxdoc.generator.RuntimeProject;
import java.io.File;
import java.util.List;
import org.apache.log4j.Logger;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.common.types.util.TypeReferences;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.pragmaticmodeling.pxdoc.enablers.capella.ICapellaInputContext;
import org.pragmaticmodeling.pxdoc.enablers.eclipse.EclipseUiPrj;
import org.pragmaticmodeling.pxdoc.enablers.eclipse.IEclipseUiContext;
import org.pragmaticmodeling.pxgen.runtime.AbstractGeneratorFragment;
import org.pragmaticmodeling.pxgen.runtime.IGeneratorFragment;
import org.pragmaticmodeling.pxgen.runtime.PxGenParameter;
import org.pragmaticmodeling.pxgen.runtime.projects.ProjectDescriptor;

@SuppressWarnings("all")
public class CapellaInput extends AbstractGeneratorFragment implements ICapellaInputContext {
  public CapellaInput() {
    super();
  }
  
  public IEclipseUiContext getParentContext() {
    return (IEclipseUiContext)getParentFragment();
  }
  
  private String selectionClassFqn;
  
  private String initSelectionClassFqn() {
    return "org.eclipse.emf.ecore.EObject";
  }
  
  @PxGenParameter
  public String getSelectionClassFqn() {
    if (selectionClassFqn == null) {
    	selectionClassFqn = initSelectionClassFqn();	
    }
    return selectionClassFqn;
  }
  
  @PxGenParameter
  public void setSelectionClassFqn(final String selectionClassFqn) {
    this.selectionClassFqn = selectionClassFqn;
  }
  
  private String label;
  
  private String initLabel() {
    String _generatorClassName = this.getGeneratorClassName();
    String _plus = ("Generate " + _generatorClassName);
    return _plus;
  }
  
  @PxGenParameter
  public String getLabel() {
    if (label == null) {
    	label = initLabel();	
    }
    return label;
  }
  
  @PxGenParameter
  public void setLabel(final String label) {
    this.label = label;
  }
  
  private String commandId;
  
  private String initCommandId() {
    String _basePackage = this.getBasePackage();
    String _plus = (_basePackage + ".ui.commands.");
    String _lowerCase = this.getGeneratorClassName().toLowerCase();
    String _plus_1 = (_plus + _lowerCase);
    return _plus_1;
  }
  
  @PxGenParameter
  public String getCommandId() {
    if (commandId == null) {
    	commandId = initCommandId();	
    }
    return commandId;
  }
  
  @PxGenParameter
  public void setCommandId(final String commandId) {
    this.commandId = commandId;
  }
  
  @PxGenParameter
  public void setPluginEntries(final List<String> pluginEntries) {
    getParentContext().setPluginEntries(pluginEntries);
  }
  
  @PxGenParameter
  public List<String> getPluginEntries() {
    return getParentContext().getPluginEntries();
  }
  
  @PxGenParameter
  public void setExecutableExtensionFactoryClassName(final String executableExtensionFactoryClassName) {
    getParentContext().setExecutableExtensionFactoryClassName(executableExtensionFactoryClassName);
  }
  
  @PxGenParameter
  public String getExecutableExtensionFactoryClassName() {
    return getParentContext().getExecutableExtensionFactoryClassName();
  }
  
  @PxGenParameter
  public void setSelectionMode(final String selectionMode) {
    getParentContext().setSelectionMode(selectionMode);
  }
  
  @PxGenParameter
  public String getSelectionMode() {
    return getParentContext().getSelectionMode();
  }
  
  @PxGenParameter
  public void setBaseCommandClass(final String baseCommandClass) {
    getParentContext().setBaseCommandClass(baseCommandClass);
  }
  
  @PxGenParameter
  public String getBaseCommandClass() {
    return getParentContext().getBaseCommandClass();
  }
  
  @PxGenParameter
  public void setCommandClassFqn(final String commandClassFqn) {
    getParentContext().setCommandClassFqn(commandClassFqn);
  }
  
  @PxGenParameter
  public String getCommandClassFqn() {
    return getParentContext().getCommandClassFqn();
  }
  
  @PxGenParameter
  public void setAbstractCommandConstructors(final String abstractCommandConstructors) {
    getParentContext().setAbstractCommandConstructors(abstractCommandConstructors);
  }
  
  @PxGenParameter
  public String getAbstractCommandConstructors() {
    return getParentContext().getAbstractCommandConstructors();
  }
  
  @PxGenParameter
  public void setAbstractCommandMethods(final String abstractCommandMethods) {
    getParentContext().setAbstractCommandMethods(abstractCommandMethods);
  }
  
  @PxGenParameter
  public String getAbstractCommandMethods() {
    return getParentContext().getAbstractCommandMethods();
  }
  
  @PxGenParameter
  public void setCommandConstructors(final String commandConstructors) {
    getParentContext().setCommandConstructors(commandConstructors);
  }
  
  @PxGenParameter
  public String getCommandConstructors() {
    return getParentContext().getCommandConstructors();
  }
  
  @PxGenParameter
  public void setMenuLabel(final String menuLabel) {
    getParentContext().setMenuLabel(menuLabel);
  }
  
  @PxGenParameter
  public String getMenuLabel() {
    return getParentContext().getMenuLabel();
  }
  
  @PxGenParameter
  public void setPropertyTesterFqn(final String propertyTesterFqn) {
    getParentContext().setPropertyTesterFqn(propertyTesterFqn);
  }
  
  @PxGenParameter
  public String getPropertyTesterFqn() {
    return getParentContext().getPropertyTesterFqn();
  }
  
  @PxGenParameter
  public void setBaseModelProvider(final String baseModelProvider) {
    getParentContext().setBaseModelProvider(baseModelProvider);
  }
  
  @PxGenParameter
  public String getBaseModelProvider() {
    return getParentContext().getBaseModelProvider();
  }
  
  @PxGenParameter
  public void setAbstractCommandClassBody(final String abstractCommandClassBody) {
    getParentContext().setAbstractCommandClassBody(abstractCommandClassBody);
  }
  
  public String getCommandObject() {
    return getParentContext().getCommandObject();
  }
  
  public JvmTypeReference findJvmRef(final String fqn) {
    return getParentContext().findJvmRef(fqn);
  }
  
  public String getAbstractCommandClassBody() {
    return getParentContext().getAbstractCommandClassBody();
  }
  
  public EclipseUiPrj getEclipseUiPrj() {
    return getParentContext().getEclipseUiPrj();
  }
  
  public String simpleName(final String qualifiedName) {
    return getParentContext().simpleName(qualifiedName);
  }
  
  public String javaPackage(final String qualifiedName) {
    return getParentContext().javaPackage(qualifiedName);
  }
  
  public String generateExtensions() {
    return getParentContext().generateExtensions();
  }
  
  @PxGenParameter
  public void setBasePackage(final String basePackage) {
    getParentContext().setBasePackage(basePackage);
  }
  
  @PxGenParameter
  public String getBasePackage() {
    return getParentContext().getBasePackage();
  }
  
  @PxGenParameter
  public void setPxDocUri(final String pxDocUri) {
    getParentContext().setPxDocUri(pxDocUri);
  }
  
  @PxGenParameter
  public String getPxDocUri() {
    return getParentContext().getPxDocUri();
  }
  
  @PxGenParameter
  public void setPxDocGenerator(final PxDocGenerator pxDocGenerator) {
    getParentContext().setPxDocGenerator(pxDocGenerator);
  }
  
  @PxGenParameter
  public void setStylesheet(final File stylesheet) {
    getParentContext().setStylesheet(stylesheet);
  }
  
  @PxGenParameter
  public File getStylesheet() {
    return getParentContext().getStylesheet();
  }
  
  @PxGenParameter
  public void setGeneratorClassName(final String generatorClassName) {
    getParentContext().setGeneratorClassName(generatorClassName);
  }
  
  @PxGenParameter
  public String getGeneratorClassName() {
    return getParentContext().getGeneratorClassName();
  }
  
  @PxGenParameter
  public void setLogger(final Logger logger) {
    getParentContext().setLogger(logger);
  }
  
  @PxGenParameter
  public Logger getLogger() {
    return getParentContext().getLogger();
  }
  
  @PxGenParameter
  public void setResourceSet(final XtextResourceSet resourceSet) {
    getParentContext().setResourceSet(resourceSet);
  }
  
  @PxGenParameter
  public XtextResourceSet getResourceSet() {
    return getParentContext().getResourceSet();
  }
  
  @PxGenParameter
  public void setTypeReferences(final TypeReferences typeReferences) {
    getParentContext().setTypeReferences(typeReferences);
  }
  
  @PxGenParameter
  public TypeReferences getTypeReferences() {
    return getParentContext().getTypeReferences();
  }
  
  public Object addDevelopmentTimeBundles(final IGeneratorFragment f) {
    return getParentContext().addDevelopmentTimeBundles(f);
  }
  
  public String getProjectName(final Class<?> clazz) {
    return getParentContext().getProjectName(clazz);
  }
  
  public RuntimeProject getRuntimeProject() {
    return getParentContext().getRuntimeProject();
  }
  
  public String getPxDocGeneratorClassName() {
    return getParentContext().getPxDocGeneratorClassName();
  }
  
  public PxDocGenerator getPxDocGenerator() {
    return getParentContext().getPxDocGenerator();
  }
  
  public PxDocGenerator loadGenerator() {
    return getParentContext().loadGenerator();
  }
  
  public Void setModelObject(final String object) {
    return getParentContext().setModelObject(object);
  }
  
  public void initialize(final Injector injector) {
    injector.injectMembers(this);
    for (ProjectDescriptor project : getProjects()) {
    	project.initialize(injector);
    }
    for (IGeneratorFragment f : getFragments()) {
    	f.initialize(injector);
    	//injector.injectMembers(f);
    }
    if (this.selectionClassFqn == null) {
    	this.selectionClassFqn = initSelectionClassFqn();
    }
    if (this.label == null) {
    	this.label = initLabel();
    }
    if (this.commandId == null) {
    	this.commandId = initCommandId();
    }
  }
  
  @Override
  public void doGenerate() {
    {
      this.setModelObject("org.polarsys.capella.common.data.modellingcore.ModelElement");
      this.setBaseModelProvider("org.pragmaticmodeling.pxdoc.runtime.ui.capella.CapellaModelProvider");
      this.setBaseCommandClass("org.pragmaticmodeling.pxdoc.runtime.ui.capella.AbstractCapellaWizardCommand");
      List<String> _requiredBundles = this.getEclipseUiPrj().getRequiredBundles();
      _requiredBundles.add("org.eclipse.ui.workbench");
      List<String> _requiredBundles_1 = this.getEclipseUiPrj().getRequiredBundles();
      _requiredBundles_1.add("org.eclipse.ui.navigator");
      List<String> _requiredBundles_2 = this.getEclipseUiPrj().getRequiredBundles();
      _requiredBundles_2.add("org.eclipse.core.resources");
      List<String> _requiredBundles_3 = this.getEclipseUiPrj().getRequiredBundles();
      _requiredBundles_3.add("org.eclipse.sirius");
      List<String> _requiredBundles_4 = this.getEclipseUiPrj().getRequiredBundles();
      _requiredBundles_4.add("org.pragmaticmodeling.pxdoc.runtime.ui.capella");
      List<String> _requiredBundles_5 = this.getEclipseUiPrj().getRequiredBundles();
      _requiredBundles_5.add("org.eclipse.gmf.runtime.diagram.ui");
      EclipseUiPrj _eclipseUiPrj = this.getEclipseUiPrj();
      _eclipseUiPrj.setBaseCommandClass("org.pragmaticmodeling.pxdoc.runtime.ui.capella.CommandHandler");
      EclipseUiPrj _eclipseUiPrj_1 = this.getEclipseUiPrj();
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("package ");
      String _basePackage = this.getBasePackage();
      _builder.append(_basePackage);
      _builder.append(".ui;");
      _builder.newLineIfNotEmpty();
      _builder.newLine();
      _builder.append("import java.util.Collection;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("import org.pragmaticmodeling.pxdoc.runtime.ui.capella.CommandHandler;");
      _builder.newLine();
      _builder.append("import org.pragmaticmodeling.pxdoc.runtime.ui.capella.IPxDocCommand;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("public abstract class Abstract");
      String _generatorClassName = this.getGeneratorClassName();
      _builder.append(_generatorClassName);
      _builder.append("Command extends ");
      String _simpleName = this.simpleName(this.getBaseCommandClass());
      _builder.append(_simpleName);
      _builder.append(" {");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("@Override");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("protected IPxDocCommand createCommand(Collection<?> selection) {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("return new ");
      String _generatorClassName_1 = this.getGeneratorClassName();
      _builder.append(_generatorClassName_1, "\t\t");
      _builder.append("CapellaCommand(selection.iterator().next());");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("}");
      _eclipseUiPrj_1.setAbstractCommandClassBody(_builder.toString());
      List<String> _requiredBundles_6 = this.getRuntimeProject().getRequiredBundles();
      _requiredBundles_6.add("org.pragmaticmodeling.pxdoc.runtime.capella");
      List<String> _requiredBundles_7 = this.getRuntimeProject().getRequiredBundles();
      _requiredBundles_7.add("org.polarsys.capella.core.data.gen;visibility:=reexport");
      List<String> _requiredBundles_8 = this.getRuntimeProject().getRequiredBundles();
      _requiredBundles_8.add("org.pragmaticmodeling.pxdoc.plugins.html2pxdoc");
      List<String> _requiredBundles_9 = this.getRuntimeProject().getRequiredBundles();
      _requiredBundles_9.add("org.pragmaticmodeling.pxdoc.plugins.diagrams");
      String basePath = this.getBasePackage().replaceAll("\\.", "/");
      // generate file CapellaCommand
      getEclipseUiPrj().getSrc().generateFile((((basePath + "/ui/") + this.getGeneratorClassName()) + "CapellaCommand.java"), new org.pragmaticmodeling.pxdoc.enablers.capella.files.CapellaCommandFile(this).getContent());
    }
  }
}
