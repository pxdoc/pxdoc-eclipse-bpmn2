package org.pragmaticmodeling.pxdoc.enablers.capella;

import org.pragmaticmodeling.pxdoc.enablers.eclipse.IEclipseUiContext;
import org.pragmaticmodeling.pxgen.runtime.PxGenParameter;

@SuppressWarnings("all")
public interface ICapellaInputContext extends IEclipseUiContext {
  @PxGenParameter
  public abstract String getSelectionClassFqn();
  
  @PxGenParameter
  public abstract void setSelectionClassFqn(final String selectionClassFqn);
  
  @PxGenParameter
  public abstract String getLabel();
  
  @PxGenParameter
  public abstract void setLabel(final String label);
  
  @PxGenParameter
  public abstract String getCommandId();
  
  @PxGenParameter
  public abstract void setCommandId(final String commandId);
}
