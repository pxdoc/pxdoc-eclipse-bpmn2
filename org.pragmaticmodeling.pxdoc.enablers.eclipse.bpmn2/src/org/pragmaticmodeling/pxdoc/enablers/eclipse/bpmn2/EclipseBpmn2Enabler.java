package org.pragmaticmodeling.pxdoc.enablers.eclipse.bpmn2;

import java.util.List;

import fr.pragmaticmodeling.pxdoc.dsl.ui.enablers.AbstractPlatformEnabler;

public class EclipseBpmn2Enabler extends AbstractPlatformEnabler {

	public EclipseBpmn2Enabler() {
		super();
	}

	@Override
	public String getJavaClass() {
		return "org.pragmaticmodeling.pxdoc.enablers.eclipse.bpmn2.EclipseBpmn2Input";
	}

	@Override
	public List<String> getDevelopmentTimesBundles() {
		List<String> result = super.getDevelopmentTimesBundles();
		result.add("org.pragmaticmodeling.pxdoc.enablers.eclipse.bpmn2");
		return result;
	}

	@Override
	public String getModelObject() {
		return "org.eclipse.bpmn2.Definitions";
	}

	@Override
	public List<String> getRequiredBundles(boolean withPxdocLibs) {
		List<String> result = super.getRequiredBundles(withPxdocLibs);
		result.add("org.pragmaticmodeling.pxdoc.runtime.eclipse.bpmn2");
		result.add("org.eclipse.bpmn2;visibility:=reexport");
		result.add("org.pragmaticmodeling.pxdoc.plugins.html2pxdoc");
		if (withPxdocLibs) {
			result.add("org.pragmaticmodeling.pxdoc.eclipse.bpmn2.lib");
			result.add("org.pragmaticmodeling.pxdoc.common.lib");
			result.add("org.pragmaticmodeling.pxdoc.emf.lib");
		}
		return result;
	}

	@Override
	public String getRootTemplateDeclarations() {
		String result = "// Commons lib configuration : set a description provider able to get information about object description\n";
		result += "descriptionProvider = new EclipseBpmn2DescriptionProvider(this)\n";
		return result;
	}

	@Override
	public List<String> getWithModules() {
		List<String> result = super.getWithModules();
		result.add("org.pragmaticmodeling.pxdoc.common.lib.CommonServices");
		result.add("org.pragmaticmodeling.pxdoc.emf.lib.EmfServices");
		return result;
	}

	@Override
	public List<String> getPxdocImports(boolean withPxdocLibs) {
		List<String> result = super.getPxdocImports(withPxdocLibs);
		if (withPxdocLibs) {
			result.add("org.pragmaticmodeling.pxdoc.eclipse.bpmn2.lib.EclipseBpmn2DescriptionProvider");
		}
		return result;
	}

}
