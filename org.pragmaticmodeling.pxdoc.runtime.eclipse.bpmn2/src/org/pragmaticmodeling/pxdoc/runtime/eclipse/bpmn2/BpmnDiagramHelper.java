package org.pragmaticmodeling.pxdoc.runtime.eclipse.bpmn2;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.bpmn2.Activity;
import org.eclipse.bpmn2.Association;
import org.eclipse.bpmn2.BaseElement;
import org.eclipse.bpmn2.DataAssociation;
import org.eclipse.bpmn2.FlowNode;
import org.eclipse.bpmn2.ItemAwareElement;
import org.eclipse.bpmn2.SequenceFlow;
import org.eclipse.bpmn2.di.BPMNDiagram;
import org.eclipse.bpmn2.di.BPMNEdge;
import org.eclipse.bpmn2.di.BPMNShape;
import org.eclipse.bpmn2.modeler.core.di.DIUtils;
import org.eclipse.bpmn2.modeler.core.utils.ErrorUtils;
import org.eclipse.bpmn2.modeler.core.utils.FileUtils;
import org.eclipse.bpmn2.modeler.ui.Bpmn2DiagramEditorInput;
import org.eclipse.bpmn2.modeler.ui.editor.BPMN2MultiPageEditor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.dd.dc.Point;
import org.eclipse.dd.di.DiagramElement;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.SWTGraphics;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.LayerConstants;
import org.eclipse.gef.editparts.LayerManager;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.ui.editor.DiagramEditor;
import org.eclipse.graphiti.ui.editor.DiagramEditorInput;
import org.eclipse.graphiti.ui.internal.parts.DiagramEditPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.pragmaticmodeling.pxdoc.images.lib.ImageHelper;

import fr.pragmaticmodeling.pxdoc.common.MeasuresConverter;

@SuppressWarnings("restriction")
public class BpmnDiagramHelper {

	private Diagram diagram;
	private BufferedImage fullImage;
	private Map<BaseElement, DiagramElement> map;
	private Map<BaseElement, Set<Association>> associations;
	private Map<BaseElement, Set<DataAssociation>> dataAssociations;
	private BPMNDiagram bpmnDiagram;
	private static final Logger logger = Logger.getLogger(BpmnDiagramHelper.class);

	public BpmnDiagramHelper(EObject eObject) {
		map = new HashMap<BaseElement, DiagramElement>();
		associations = new HashMap<BaseElement, Set<Association>>();
		dataAssociations = new HashMap<BaseElement, Set<DataAssociation>>();
		URI uri = eObject.eResource().getURI();
		String diagramName = FileUtils.createTempName(uri.toString());
		URI diagramUri = URI.createFileURI(diagramName);
		Bpmn2DiagramEditorInput input = new Bpmn2DiagramEditorInput(uri, diagramUri,
				"org.eclipse.bpmn2.modeler.ui.diagram.MainBPMNDiagramType");
		BPMN2MultiPageEditor editor2 = openEditor(input);
		if (editor2 != null) {
			bpmnDiagram = editor2.getDesignEditor().getBpmnDiagram();
			diagram = DIUtils.getOrCreateDiagram(editor2.getDesignEditor().getDiagramBehavior(), bpmnDiagram);
			fullImage = convertDiagramToBytes((DiagramEditor) editor2.getDesignEditor(), diagram, SWT.IMAGE_PNG);
			visitDiagram();
			// int availableHeight =
			// MeasuresConverter.getMeasureValueInPixels(availableHeightInTwips,
			// "TWIPS", availableWidthInTwips);
			// int availableWidth =
			// MeasuresConverter.getMeasureValueInPixels(availableWidthInTwips,
			// "TWIPS", availableWidthInTwips);
			// image = convertDiagramToBytes((DiagramEditor)
			// editor2.getDesignEditor(), diagram, SWT.IMAGE_PNG,
			// availableWidth, availableHeight, split);
		}
	}

	private void visitDiagram() {
		Iterator<EObject> it = bpmnDiagram.eAllContents();
		while (it.hasNext()) {
			EObject next = it.next();
			if (next instanceof BPMNShape) {
				BPMNShape shape = (BPMNShape) next;
				BaseElement baseElement = shape.getBpmnElement();
				if (baseElement != null) {
					map.put(baseElement, shape);
				}
			} else if (next instanceof BPMNEdge) {
				BPMNEdge edge = (BPMNEdge) next;
				BaseElement baseElement = edge.getBpmnElement();
				if (baseElement != null) {
					map.put(baseElement, edge);
					if (baseElement instanceof Association) {
						Association asso = (Association) baseElement;
						if (!associations.containsKey(asso.getSourceRef())) {
							associations.put(asso.getSourceRef(), new HashSet<Association>());
						}
						associations.get(asso.getSourceRef()).add(asso);
						if (!associations.containsKey(asso.getTargetRef())) {
							associations.put(asso.getTargetRef(), new HashSet<Association>());
						}
						associations.get(asso.getTargetRef()).add(asso);
					} else if (baseElement instanceof DataAssociation) {
						DataAssociation asso = (DataAssociation) baseElement;
						for (ItemAwareElement ref : asso.getSourceRef()) {
							if (!dataAssociations.containsKey(ref)) {
								dataAssociations.put(ref, new HashSet<DataAssociation>());
							}
							dataAssociations.get(ref).add(asso);
						}
						if (!dataAssociations.containsKey(asso.getTargetRef())) {
							dataAssociations.put(asso.getTargetRef(), new HashSet<DataAssociation>());
						}
						dataAssociations.get(asso.getTargetRef()).add(asso);
					}
				}
			}
		}
	}

	public BufferedImage exportSingleDiagram() {
		return fullImage;
	}

	public List<BufferedImage> exportDiagram(int availableWidthInTwips, int availableHeightInTwips, boolean split) {
		List<BufferedImage> result = new ArrayList<BufferedImage>();
		try {
			int availableHeight = MeasuresConverter.getMeasureValueInPixels(availableHeightInTwips, "TWIPS",
					availableWidthInTwips);
			int availableWidth = MeasuresConverter.getMeasureValueInPixels(availableWidthInTwips, "TWIPS",
					availableWidthInTwips);
			if (!split) {
				result.add(fullImage);
			} else {
				result.addAll(ImageHelper.splitImage(fullImage, availableWidth, availableHeight));
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return result;
	}

	public BufferedImage convertDiagramToBytes(DiagramEditor diagramEditor, Diagram diagram, int format) {

		final List<BufferedImage> result = new ArrayList<BufferedImage>();

		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {

				// Set the Graphiti diagram bounds...
				int width = diagram.getGraphicsAlgorithm().getWidth(),
						height = diagram.getGraphicsAlgorithm().getHeight();
				IFigure diagramFigure = ((LayerManager) diagramEditor.getGraphicalViewer().getRootEditPart())
						.getLayer(LayerConstants.PRINTABLE_LAYERS);
				diagramFigure.setBounds(new Rectangle(0, 0, width, height));
				// ...and trigger the figures validation
				diagramFigure.validate();

				// Set bounds to final narrowed image size
				DiagramEditPart diagramEditPart = (DiagramEditPart) diagramEditor.getGraphicalViewer().getRootEditPart()
						.getChildren().get(0);
				int XMin = width;
				int YMin = height;
				int XMax = 0;
				int YMax = 0;
				for (Object o : diagramEditPart.getFigure().getChildren()) {
					Figure f = (Figure) o;
					if (f.getLocation().x < XMin) {
						XMin = f.getLocation().x;
					}
					if (f.getLocation().y < YMin) {
						YMin = f.getLocation().y;
					}
					if (f.getLocation().x + f.getBounds().width > XMax) {
						XMax = f.getLocation().x + f.getBounds().width;
					}
					if (f.getLocation().y + f.getBounds().height > YMax) {
						YMax = f.getLocation().y + f.getBounds().height;
					}
				}

				width = XMin + XMax;
				height = YMin + YMax;
				// Workaround for default diagram creation limited to 1000 x
				// 1000 bounds
				if (width > diagramFigure.getBounds().width || height > diagramFigure.getBounds().height) {
					diagramFigure.setBounds(new Rectangle(0, 0, width, height));
					diagramFigure.validate();
				}
				Image diagramImage = new Image(Display.getDefault(), width, height);
				GC gc = new GC(diagramImage);
				SWTGraphics graphics = new SWTGraphics(gc);
				diagramFigure.paint(graphics);
				graphics.dispose();
				gc.dispose();
				// if (!split) {
				result.add(ImageHelper.convertToAWT(diagramImage));
				// } else {
				// result.addAll(ImageHelper.splitImage(ImageHelper.convertToAWT(diagramImage),
				// availableWidth, availableHeight));
				// }
			}
		});

		return result.get(0);
	}

	private static BPMN2MultiPageEditor openEditor(final DiagramEditorInput editorInput) {
		final Object result[] = { new Object() };
		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {
			@Override
			public void run() {
				try {
					IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
					IEditorPart part = null;
					part = page.findEditor(editorInput);
					if (part != null) {
						page.activate(part);
					} else {
						part = page.openEditor(editorInput, "org.eclipse.bpmn2.modeler.ui.bpmn2editor");
					}
					result[0] = part;
				} catch (PartInitException e) {
					String error = "Could not open BPMN editor";
					IStatus status = new Status(IStatus.ERROR, "null", error, e);
					ErrorUtils.showErrorWithLogging(status);
				}
			}
		});
		return (BPMN2MultiPageEditor) result[0];
	}

	public BufferedImage cropImage(FlowNode element) {
		int imageWidth = fullImage.getWidth();
		int imageHeight = fullImage.getHeight();
		BPMNShape diElt = (BPMNShape) map.get(element);
		if (diElt != null) {
			Set<DiagramElement> involved = new HashSet<DiagramElement>();
			involved.add(diElt);
			involved.addAll(diElt.getOwnedLabel());
			includeAssociations(element, involved);

			for (SequenceFlow flow : element.getIncoming()) {
				BPMNEdge edge = (BPMNEdge) map.get(flow);
				involved.add(edge);
				involved.addAll(edge.getOwnedLabel());
				FlowNode sourceRef = flow.getSourceRef();
				BPMNShape sourceShape = (BPMNShape) map.get(sourceRef);
				involved.add(sourceShape);
				involved.addAll(sourceShape.getOwnedLabel());
				includeAssociations(sourceRef, involved);
			}
			for (SequenceFlow flow : element.getOutgoing()) {
				BPMNEdge edge = (BPMNEdge) map.get(flow);
				involved.add(edge);
				FlowNode targetRef = flow.getTargetRef();
				BPMNShape targetShape = (BPMNShape) map.get(targetRef);
				involved.add(targetShape);
				involved.addAll(targetShape.getOwnedLabel());
				includeAssociations(targetRef, involved);
			}
			int minX = 999999;
			int minY = 999999;
			int maxX = 0;
			int maxY = 0;
			for (DiagramElement diElement : involved) {
				if (diElement instanceof BPMNShape) {
					BPMNShape shape = (BPMNShape) diElement;
					int x = (int) shape.getBounds().getX();
					int y = (int) shape.getBounds().getY();
					int x2 = x + (int) shape.getBounds().getWidth();
					int y2 = y + (int) shape.getBounds().getHeight();
					if (x < minX) {
						if (x < 0)
							x = 0;
						minX = x;
					}
					if (x2 > maxX) {
						if (x2 > imageWidth)
							x2 = imageWidth;
						maxX = x2;
					}
					if (y < minY) {
						if (y < 0)
							y = 0;
						minY = y;
					}
					if (y2 > maxY) {
						if (y2 > imageHeight)
							y2 = imageHeight;
						maxY = y2;
					}
				} else if (diElement instanceof BPMNEdge) {
					BPMNEdge edge = (BPMNEdge) diElement;
					for (Point p : edge.getWaypoint()) {
						int x = (int) p.getX();
						int y = (int) p.getY();
						if (x < minX) {
							if (x < 0)
								x = 0;
							minX = x;
						}
						if (x > maxX) {
							if (x > imageWidth)
								x = imageWidth;
							maxX = x;
						}
						if (y < minY) {
							if (y < 0)
								y = 0;
							minY = y;
						}
						if (y > maxY) {
							if (y > imageHeight)
								y = imageHeight;
							maxY = y;
						}
					}
				}
			}
			int width = maxX - minX;
			int height = maxY - minY;
			java.awt.Rectangle rect = new java.awt.Rectangle(minX, minY, width, height);
			return ImageHelper.cropImage(fullImage, rect);
		}
		return null;
	}

	private void includeAssociations(FlowNode element, Set<DiagramElement> involved) {
		basicIncludeAssociations(element, involved);
		if (element instanceof Activity) {
			Activity activity = (Activity) element;
			for (DataAssociation asso : activity.getDataInputAssociations()) {
				involved.add(map.get(asso));
				for (ItemAwareElement ref : asso.getSourceRef()) {
					involved.add(map.get(ref));
				}
				involved.add(map.get(asso.getTargetRef()));
			}
			for (DataAssociation asso : activity.getDataOutputAssociations()) {
				involved.add(map.get(asso));
				for (ItemAwareElement ref : asso.getSourceRef()) {
					involved.add(map.get(ref));
				}
				involved.add(map.get(asso.getTargetRef()));
			}
		}
	}

	private void basicIncludeAssociations(FlowNode element, Set<DiagramElement> involved) {
		if (associations.containsKey(element)) {
			for (Association asso : associations.get(element)) {
				involved.add(map.get(asso));
				involved.add(map.get(asso.getSourceRef()));
				involved.add(map.get(asso.getTargetRef()));
			}
		}
		// if (dataAssociations.containsKey(element)) {
		// for (DataAssociation asso : dataAssociations.get(element)) {
		// involved.add(map.get(asso));
		// for (ItemAwareElement ref : asso.getSourceRef()) {
		// involved.add(map.get(ref));
		// }
		// involved.add(map.get(asso.getTargetRef()));
		// }
		// }
	}
}
